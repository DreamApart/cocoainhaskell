{-# LANGUAGE ForeignFunctionInterface #-}

module Utility where

import Foreign
import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Array

foreign import ccall unsafe "get_resource_path"
    c_get_resource_file :: Ptr CChar -> CInt -> CString -> CString -> IO ()

getResourceFile :: (String, String) -> IO String
getResourceFile (name, ext) = do
    path <-
        withCString name $ \pName ->
        withCString ext $ \pExt ->
        allocaArray defautBufferSize $ \pResult -> do
            c_get_resource_file pResult (defautBufferSize::CInt) pName pExt
            peekCString pResult
    readFile path

defautBufferSize :: Integral a => a
defautBufferSize = 2048