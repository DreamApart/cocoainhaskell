all: $(BUILT_PRODUCTS_DIR)/$(EXECUTABLE_PATH)

$(BUILT_PRODUCTS_DIR)/$(EXECUTABLE_PATH): $(DERIVED_FILE_DIR)/executable
	mkdir -p $(BUILT_PRODUCTS_DIR)/$(EXECUTABLE_FOLDER_PATH)
	mv $(DERIVED_FILE_DIR)/executable $(BUILT_PRODUCTS_DIR)/$(EXECUTABLE_PATH)

$(DERIVED_FILE_DIR)/executable: $(DERIVED_FILE_DIR)/Utility.mm.o
	mkdir -p $(DERIVED_FILE_DIR)
	cd Haskell && \
    ghc -O -O3 -odir $(DERIVED_FILE_DIR) -hidir $(DERIVED_FILE_DIR) \
        -framework Cocoa -lc++ \
        -o $(DERIVED_FILE_DIR)/executable $(DERIVED_FILE_DIR)/Utility.mm.o Main

$(DERIVED_FILE_DIR)/Utility.mm.o: HsTest/Utility.mm
	clang++ -std=c++14 -c -ObjC++ -o $@ $<

.PHONY: $(DERIVED_FILE_DIR)/executable
.PHONY: all


