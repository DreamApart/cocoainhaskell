#import <cstring>
#import <Foundation/Foundation.h>

extern "C" {
    void get_resource_path(char* output, int size, const char* filename, const char* extname);
}

void get_resource_path(char* output, int size, const char* filename, const char* extname) {
    NSString* _filename = [NSString stringWithCString:filename encoding:NSASCIIStringEncoding];
    NSString* _extname = [NSString stringWithCString:extname encoding:NSASCIIStringEncoding];
    NSString* _filepath = [[NSBundle mainBundle] pathForResource:_filename ofType:_extname];
    
    const char* path = _filepath.UTF8String;
    
    strncpy(output, path, size);
}
